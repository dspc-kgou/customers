/****************************************************************************
 *
 *		Target Tuning Symbol File
 *		-------------------------
 *
 * 		This file is populated with symbol information only for modules
 *		that have the 'isTunable' property checked in the .awd.
 *
 *          Generated on:  04-Oct-2019 15:01:47
 *
 ***************************************************************************/

#ifndef MICROPHONES_SENSITIVITY_CALIBRATIONSIMPLE_H
#define MICROPHONES_SENSITIVITY_CALIBRATIONSIMPLE_H

// ----------------------------------------------------------------------
// RouteMics [Router]
// Channel routing without smoothing

#define AWE_RouteMics_ID 30003

// int channelIndex[8] - Specifies input to output channel routing. Each 
//         item in the array corresponds to an output channel and contains two 
//         packed values: input pin number in the high 16 bits; input channel 
//         number in the low 16 bits.
// Default value:
//     0
//     1
//     2
//     3
//     4
//     5
//     6
//     7
// Range: unrestricted
#define AWE_RouteMics_channelIndex_OFFSET 8
#define AWE_RouteMics_channelIndex_MASK 0x00000100
#define AWE_RouteMics_channelIndex_SIZE 8


// ----------------------------------------------------------------------
// ScalerN1 [ScalerNV2]
// General purpose scaler with separate gains per channel

#define AWE_ScalerN1_ID 30000

// float masterGain - Overall gain to apply.
// Default value: 0
// Range: -24 to 24
#define AWE_ScalerN1_masterGain_OFFSET 8
#define AWE_ScalerN1_masterGain_MASK 0x00000100
#define AWE_ScalerN1_masterGain_SIZE -1

// float smoothingTime - Time constant of the smoothing process (0 = 
//         unsmoothed).
// Default value: 10
// Range: 0 to 1000
#define AWE_ScalerN1_smoothingTime_OFFSET 9
#define AWE_ScalerN1_smoothingTime_MASK 0x00000200
#define AWE_ScalerN1_smoothingTime_SIZE -1

// int isDB - Selects between linear (=0) and dB (=1) operation
// Default value: 1
// Range: unrestricted
#define AWE_ScalerN1_isDB_OFFSET 10
#define AWE_ScalerN1_isDB_MASK 0x00000400
#define AWE_ScalerN1_isDB_SIZE -1

// float smoothingCoeff - Smoothing coefficient.
#define AWE_ScalerN1_smoothingCoeff_OFFSET 11
#define AWE_ScalerN1_smoothingCoeff_MASK 0x00000800
#define AWE_ScalerN1_smoothingCoeff_SIZE -1

// float trimGain[8] - Array of trim gains, one per channel
// Default value:
//     0
//     0
//     0
//     0
//     0
//     0
//     0
//     0
// Range: -24 to 24
#define AWE_ScalerN1_trimGain_OFFSET 12
#define AWE_ScalerN1_trimGain_MASK 0x00001000
#define AWE_ScalerN1_trimGain_SIZE 8

// float targetGain[8] - Computed target gains in linear units
#define AWE_ScalerN1_targetGain_OFFSET 13
#define AWE_ScalerN1_targetGain_MASK 0x00002000
#define AWE_ScalerN1_targetGain_SIZE 8

// float currentGain[8] - Instanteous gains.  These ramp towards 
//         targetGain
#define AWE_ScalerN1_currentGain_OFFSET 14
#define AWE_ScalerN1_currentGain_MASK 0x00004000
#define AWE_ScalerN1_currentGain_SIZE 8


// ----------------------------------------------------------------------
// MicSensitivity [ScaleOffset]
// Scales a signal and then adds an offset

#define AWE_MicSensitivity_ID 30001

// float gain - Linear gain.
// Default value: -1
// Range: -10 to 10
#define AWE_MicSensitivity_gain_OFFSET 8
#define AWE_MicSensitivity_gain_MASK 0x00000100
#define AWE_MicSensitivity_gain_SIZE -1

// float offset - DC offset.
// Default value: -28
// Range: -100 to 100
#define AWE_MicSensitivity_offset_OFFSET 9
#define AWE_MicSensitivity_offset_MASK 0x00000200
#define AWE_MicSensitivity_offset_SIZE -1


// ----------------------------------------------------------------------
// Sink_SPL [Sink]
// Copies the data at the input pin and stores it in an internal buffer.

#define AWE_Sink_SPL_ID 30002

// int enable - To enable or disable the plotting.
// Default value: 0
// Range: unrestricted
#define AWE_Sink_SPL_enable_OFFSET 8
#define AWE_Sink_SPL_enable_MASK 0x00000100
#define AWE_Sink_SPL_enable_SIZE -1

// float value[8] - Captured values.
#define AWE_Sink_SPL_value_OFFSET 9
#define AWE_Sink_SPL_value_MASK 0x00000200
#define AWE_Sink_SPL_value_SIZE 8

// float yRange[2] - Y-axis range.
// Default value:
//     -5  5
// Range: unrestricted
#define AWE_Sink_SPL_yRange_OFFSET 10
#define AWE_Sink_SPL_yRange_MASK 0x00000400
#define AWE_Sink_SPL_yRange_SIZE 2



#endif // MICROPHONES_SENSITIVITY_CALIBRATIONSIMPLE_H

