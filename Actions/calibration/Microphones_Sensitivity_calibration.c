/****************************************************************************
 *
 *		Target Tuning Symbol File
 *		-------------------------
 *
 *          Generated on:  04-Oct-2019 15:01:47
 *
 ***************************************************************************/

#include "Microphones_Sensitivity_calibration.h"
AWE_MOD_SLOW_ANY_DATA WireInstance *wire1;
AWE_MOD_SLOW_ANY_DATA WireInstance *wire2;
AWE_MOD_SLOW_ANY_DATA WireInstance *wire3;
AWE_MOD_SLOW_ANY_DATA WireInstance *wire4;
AWE_MOD_SLOW_ANY_DATA WireInstance *wire5;
AWE_MOD_SLOW_ANY_DATA WireInstance *wire6;
AWE_MOD_SLOW_ANY_DATA WireInstance *wire7;
AWE_MOD_SLOW_ANY_DATA WireInstance *wire8;
AWE_MOD_SLOW_ANY_DATA WireInstance *wire9;
AWE_MOD_SLOW_ANY_DATA awe_modTypeConversionClass *SYS_toFloat;
AWE_MOD_SLOW_ANY_DATA awe_modMeterClass *Meter_All;
AWE_MOD_SLOW_ANY_DATA awe_modSecondOrderFilterSmoothedClass *DC_Noise_Filter3;
AWE_MOD_SLOW_ANY_DATA awe_modMeterClass *Meter_Mic;
AWE_MOD_SLOW_ANY_DATA awe_modAudioWeightingClass *AudioWeighting1;
AWE_MOD_SLOW_ANY_DATA awe_modBiquadCascadeClass *AudioWeighting1_filt;
AWE_MOD_SLOW_ANY_DATA awe_modMuteUnmuteClass *AudioWeighting1_mute;
AWE_MOD_SLOW_ANY_DATA awe_modRMSNClass *RMS1;
AWE_MOD_SLOW_ANY_DATA awe_modDb20Class *Db201;
AWE_MOD_SLOW_ANY_DATA awe_modScaleOffsetClass *ConvertToSPL;
AWE_MOD_SLOW_ANY_DATA awe_modZeroSourceClass *ZeroSource1;
AWE_MOD_SLOW_ANY_DATA awe_modTypeConversionClass *SYS_toFract;
AWE_MOD_SLOW_ANY_DATA awe_modScalerNV2Class *ScalerN1;
AWE_MOD_SLOW_ANY_DATA awe_modScaleOffsetClass *MicSensitivity;
AWE_MOD_SLOW_ANY_DATA awe_modSinkClass *Sink_SPL;
AWE_MOD_SLOW_ANY_DATA awe_modRouterClass *RouteMics;
AWE_MOD_SLOW_ANY_DATA LayoutInstance *theLayout1;

#ifdef __cplusplus
extern "C" {
#endif

#include "Errors.h"

/* ----------------------------------------------------------------------
** Object variables declarations.
** ------------------------------------------------------------------- */
    
INT32 InitializeAWESymbols(void)
{

InstanceDescriptor *inst;
UINT32 classID;
    
if (awe_fwGetObjectByID(wire1_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
wire1 = (WireInstance *) inst;

if (awe_fwGetObjectByID(wire2_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
wire2 = (WireInstance *) inst;

if (awe_fwGetObjectByID(wire3_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
wire3 = (WireInstance *) inst;

if (awe_fwGetObjectByID(wire4_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
wire4 = (WireInstance *) inst;

if (awe_fwGetObjectByID(wire5_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
wire5 = (WireInstance *) inst;

if (awe_fwGetObjectByID(wire6_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
wire6 = (WireInstance *) inst;

if (awe_fwGetObjectByID(wire7_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
wire7 = (WireInstance *) inst;

if (awe_fwGetObjectByID(wire8_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
wire8 = (WireInstance *) inst;

if (awe_fwGetObjectByID(wire9_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
wire9 = (WireInstance *) inst;

if (awe_fwGetObjectByID(SYS_toFloat_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
SYS_toFloat = (awe_modTypeConversionClass *) inst;

if (awe_fwGetObjectByID(Meter_All_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
Meter_All = (awe_modMeterClass *) inst;

if (awe_fwGetObjectByID(DC_Noise_Filter3_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
DC_Noise_Filter3 = (awe_modSecondOrderFilterSmoothedClass *) inst;

if (awe_fwGetObjectByID(Meter_Mic_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
Meter_Mic = (awe_modMeterClass *) inst;

if (awe_fwGetObjectByID(AudioWeighting1_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
AudioWeighting1 = (awe_modAudioWeightingClass *) inst;

if (awe_fwGetObjectByID(AudioWeighting1_filt_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
AudioWeighting1_filt = (awe_modBiquadCascadeClass *) inst;

if (awe_fwGetObjectByID(AudioWeighting1_mute_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
AudioWeighting1_mute = (awe_modMuteUnmuteClass *) inst;

if (awe_fwGetObjectByID(RMS1_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
RMS1 = (awe_modRMSNClass *) inst;

if (awe_fwGetObjectByID(Db201_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
Db201 = (awe_modDb20Class *) inst;

if (awe_fwGetObjectByID(ConvertToSPL_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
ConvertToSPL = (awe_modScaleOffsetClass *) inst;

if (awe_fwGetObjectByID(ZeroSource1_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
ZeroSource1 = (awe_modZeroSourceClass *) inst;

if (awe_fwGetObjectByID(SYS_toFract_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
SYS_toFract = (awe_modTypeConversionClass *) inst;

if (awe_fwGetObjectByID(ScalerN1_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
ScalerN1 = (awe_modScalerNV2Class *) inst;

if (awe_fwGetObjectByID(MicSensitivity_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
MicSensitivity = (awe_modScaleOffsetClass *) inst;

if (awe_fwGetObjectByID(Sink_SPL_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
Sink_SPL = (awe_modSinkClass *) inst;

if (awe_fwGetObjectByID(RouteMics_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
RouteMics = (awe_modRouterClass *) inst;

if (awe_fwGetObjectByID(theLayout1_ID, &inst, &classID) != E_SUCCESS)
    return( E_CLASS_NOT_FOUND);
theLayout1 = (LayoutInstance *) inst;

        
return(E_SUCCESS);
}

#ifdef __cplusplus
}
#endif

