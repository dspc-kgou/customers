/****************************************************************************
 *
 *		Target Tuning Symbol File
 *		-------------------------
 *
 *          Generated on:  04-Oct-2019 15:01:47
 *
 ***************************************************************************/

#ifndef MICROPHONES_SENSITIVITY_CALIBRATION_H
#define MICROPHONES_SENSITIVITY_CALIBRATION_H

#include "Framework.h"

#include "ModAudioWeighting.h"
#include "ModBiquadCascade.h"
#include "ModDb20.h"
#include "ModMeter.h"
#include "ModMuteUnmute.h"
#include "ModRMSN.h"
#include "ModRouter.h"
#include "ModScaleOffset.h"
#include "ModScalerNV2.h"
#include "ModSecondOrderFilterSmoothed.h"
#include "ModSink.h"
#include "ModTypeConversion.h"
#include "ModZeroSource.h"

#define wire1_ID 1
#define wire2_ID 2
#define wire3_ID 3
#define wire4_ID 4
#define wire5_ID 5
#define wire6_ID 6
#define wire7_ID 7
#define wire8_ID 8
#define wire9_ID 9
#define SYS_toFloat_ID 10
#define Meter_All_ID 11
#define DC_Noise_Filter3_ID 13
#define Meter_Mic_ID 14
#define AudioWeighting1_ID 16
#define AudioWeighting1_filt_ID 17
#define AudioWeighting1_mute_ID 18
#define RMS1_ID 19
#define Db201_ID 20
#define ConvertToSPL_ID 22
#define ZeroSource1_ID 24
#define SYS_toFract_ID 25
#define ScalerN1_ID 30000
#define MicSensitivity_ID 30001
#define Sink_SPL_ID 30002
#define RouteMics_ID 30003
#define theLayout1_ID 26

INT32 InitializeAWESymbols(void);

/* ----------------------------------------------------------------------
** Object variables declarations.
** ------------------------------------------------------------------- */

extern WireInstance *wire1;
extern WireInstance *wire2;
extern WireInstance *wire3;
extern WireInstance *wire4;
extern WireInstance *wire5;
extern WireInstance *wire6;
extern WireInstance *wire7;
extern WireInstance *wire8;
extern WireInstance *wire9;
extern awe_modTypeConversionClass *SYS_toFloat;
extern awe_modMeterClass *Meter_All;
extern awe_modSecondOrderFilterSmoothedClass *DC_Noise_Filter3;
extern awe_modMeterClass *Meter_Mic;
extern awe_modAudioWeightingClass *AudioWeighting1;
extern awe_modBiquadCascadeClass *AudioWeighting1_filt;
extern awe_modMuteUnmuteClass *AudioWeighting1_mute;
extern awe_modRMSNClass *RMS1;
extern awe_modDb20Class *Db201;
extern awe_modScaleOffsetClass *ConvertToSPL;
extern awe_modZeroSourceClass *ZeroSource1;
extern awe_modTypeConversionClass *SYS_toFract;
extern awe_modScalerNV2Class *ScalerN1;
extern awe_modScaleOffsetClass *MicSensitivity;
extern awe_modSinkClass *Sink_SPL;
extern awe_modRouterClass *RouteMics;
extern LayoutInstance *theLayout1;

#endif // MICROPHONES_SENSITIVITY_CALIBRATION_H

